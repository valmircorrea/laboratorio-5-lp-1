#Makefile for "laboratorio-05" C++ application
#Created by Valmir Correa 27/04/2017

RM = rm -rf

# Compilador:
CC = g++

# Variaveis para os subdiretorios:

INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
OUT_DIR=./data/output

# Opcoes de compilacao:
CFLAGS = -Wall -pedantic -ansi -std=c++14 -I. -I$(INC_DIR)/Programa_1

# Assegura que os alvos não sejam confundidos com os arquivos de mesmo nome:
.PHONY: all clean distclean doxy

# Define o alvo para a compilação completa:
all: Programa_1

debug: CFLAGS += -g -O0
debug: Programa_1

# Alvo para a contrução do executavel Programa_1:
Programa_1: $(OBJ_DIR)/main.o $(OBJ_DIR)/empresa.o $(OBJ_DIR)/funcionario.o $(OBJ_DIR)/dados_empresa.o $(OBJ_DIR)/menu.o
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^

# Alvo para a construção do main.o:
$(OBJ_DIR)/main.o: $(SRC_DIR)/Programa_1/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do empresa.o:
$(OBJ_DIR)/empresa.o: $(SRC_DIR)/Programa_1/empresa.cpp $(INC_DIR)/Programa_1/empresa.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do funcionario.o:
$(OBJ_DIR)/funcionario.o: $(SRC_DIR)/Programa_1/funcionario.cpp $(INC_DIR)/Programa_1/funcionario.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do dados_empresa.o:
$(OBJ_DIR)/dados_empresa.o: $(SRC_DIR)/Programa_1/dados_empresa.cpp $(INC_DIR)/Programa_1/dados_empresa.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do menu.o:
$(OBJ_DIR)/menu.o:$(SRC_DIR)/Programa_1/menu.cpp $(INC_DIR)/Programa_1/menu.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a execução do Valgrind:
valgrind:
	valgrind --leak-check=full --show-reachable=yes ./bin/Programa_1

# Alvo para a geração automatica de documentacao usando o Doxygen:
doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile
doxyg:
	doxygen -g

# Alvo usado para limpar os arquivos temporarios (objeto):
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
	$(RM) $(OUT_DIR)/*
