/**
* @file	    dados_empresa.h
* @brief	Arquivo com a assinatura das funções de manipulação de dados das empresas.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	04/04/2017
* @date 	07/04/2017
*/

#ifndef DADOS_EMPRESA_H_
#define DADOS_EMPRESA_H_

/**
* @brief  Função que cadastra uma nova empresa.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  *qtd_empresas Ponteiro de inteiro que recebe a quantidade de empresas cadastradas até o momento.
* @return  Retorna uma nova empresa já cadastrada.
*/
Empresa* cadastrar_empresa (Empresa *empresa, int *qtd_empresas);

/**
* @brief  Função que cadastra uma novo funcionário.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  indice_aux Variável que recebe um índice para saber em qual empresa cadastrar o novo funcionário.
* @return Retorna 0.
*/
int cadastrar_funcionarios ( Empresa *empresa, int indice_aux);

/**
* @brief  Função que adiciona uma nova empresa, manipulando adequadamente a alocação dinâmica.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  *qtd_empresas Ponteiro de inteiro que recebe a quantidade de empresas cadastradas até o momento.
* @return Retorna um novo vetor com um numero de empresas atualizado.
*/
Empresa* add_empresa (Empresa *empresa, int *qtd_empresas);

/**
* @brief  Função que realiza uma busca de uma determinada empresa no banco de dados.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  *qtd_empresas Ponteiro de inteiro que recebe a quantidade de empresas cadastradas até o momento.
* @return  Retorna o índice da posição de onde está a empresa solicitado pelo usuário.
*/
int Busca_empresa (Empresa *empresa, int n, int qtd_empresas);

/**
* @brief   Função que lista as empresas cadastradas pelo usuário.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  *qtd_empresas Ponteiro de inteiro que recebe a quantidade de empresas cadastradas até o momento.
*/
void Listar_empresas (Empresa *empresa, int qtd_empresas);

#endif