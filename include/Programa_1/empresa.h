/**
* @file	    empresa.h
* @brief	Implementacao dos metodos da classe Empresa.
* @author   Valmir Correa (valmircorrea96@outlook.com) 
* @since	04/05/2017
* @date	    07/05/2017
*/

#include <iostream>
using std:: cout;
using std:: endl;

#include <string>
using std:: string;

#include "funcionario.h"

#ifndef EMPRESA_H_
#define EMPRESA_H_

/**
* @class   Empresa empresa.h
* @brief   Classe que representa o objeto Empresa;
*/
class Empresa {

    private:

    string nome_empresa;
    string CNPJ;
    Funcionario *lista_funcionarios;
    int quant;

    public:

    /**
    * @brief Contrutor para a classe Empresa.
    */
    Empresa ();

    /**
    * @brief Método para ter acesso ao nome da empresa.
    * @return Nome da empresa
    */
    string getNome_empresa ();

    /**
    * @brief  Método que atribui o nome da empresa.
    * @param  n Recebe o nome da empresa.
    */
    void setNome_empresa (string n);

    /**
    * @brief Método para ter acesso ao CNPJ da empresa.
    * @return CNPJ da empresa
    */
    string getCNPJ ();

    /**
    * @brief  Método que atribui o CNPJ da empresa.
    * @param  n Recebe o CNPJ da empresa.
    */
    void setCNPJ (string d);

    /**
    * @brief  Método que gera a lista de funcionários da empresa.
    * @param  opcao Variável que recebe a opção do usuário em relação ao tipo de filtro que será aplicado na geração da lista.
    */
    void getLista_funcionarios (int opcao);

    /**
    * @brief  Método que adiciona um novo funcionário a lista de funcionários da empresa.
    * @param  novo_funcionario Variável que recebe a nova quantidade de funcionarios.
    */
    void setLista_funcionarios(int novo_funcionario);

    /**
    * @brief  Método que adiciona um novo funcionário a empresa.
    * @param  n Recebe o nome do novo funcionário contratado.
    * @param  s Recebe o salário do novo funcionário contratado.
    * @param  d Recebe a data de admissão do novo funcionário contratado.
    */
    void add_funcionario(string nome, string sal, string data);

    /**
    * @brief  Método que verifica se o novo funcionário já foi cadastrado na empresa anteriormente.
    * @param  n Recebe o nome do novo funcionário contratado.
    * @param  s Recebe o salário do novo funcionário contratado.
    * @param  d Recebe a data de admissão do novo funcionário contratado.
    * @return True ou False, se o funcionário já foi ou não cadastrado.
    */
    bool Verificar_funcionario (string n, string s, string d);

    /**
    * @brief  Método que busca e imprime os funcionários que estão em período de experiência.
    * @param  d Recebe o dia que o funcionário foi contratado.
    * @param  m Recebe o mês que o funcionário foi contratado.
    * @param  a Recebe o ano que o funcionário foi contratado.
    */
    void FuncionarioExperiencia (int d, int m, int a);

    /**
    * @brief  Método que busca e imprime os funcionários que estão em período de experiência.
    * @param  nome Recebe o nome da empresa que o usuário quer a aplicação do aumento para os funcionários.
    * @param  n Recebe um valor para verificar se a empresa que o usuário buscou foi realmente cadastrada.
    */
    void Aumento_funcionarios (string nome, int n);

    /**
    * @brief  Método que gera um arquivo com todos os dados da empresa, especificada pelo usuário, e de seus funcionários. 
    * @param  nome Recebe o nome da empresa que o usuário quer que seja feita a geração do arquivos de dados.
    * @param  n Recebe um valor para verificar se a empresa que o usuário buscou foi realmente cadastrada.
    */
    void Salvar_dados (string nome, int n);

    /**
    * @brief Destrutor padrão para liberar o espaço na memória.
    */
    ~Empresa ();

};

#endif