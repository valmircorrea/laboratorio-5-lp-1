/**
* @file	    funcionario.h
* @brief	Definicao da classe Funcionario, atributos e seus metodos.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	03/05/2017
* @date	    07/05/2017
*/

#include <string>
using std:: string;

#include <ostream>
using std::ostream;

#ifndef FUNCIONARIO_H_
#define FUNCIONARIO_H_

/**
* @class   Funcionario funcionario.h
* @brief   Classe que representa o objeto Funcionario;
*/ 
class Funcionario {

    private:

    string nome;
    string salario;
    string data_admissao;

    public:
    
    /**
    * @brief Contrutor para a classe Funcionário.
    */
    Funcionario();

    /**
    * @brief Método para ter acesso ao nome do Funcionário.
    * @return Nome do funcionario 
    */
    string getNome ();

    /**
    * @brief   Método que atribui o nome do funcionario.
    * @param   n Recebe o nome do funcionário.
    */
    void setNome (string n);

    /**
    * @brief  Método para ter acesso ao salário do Funcionário.
    * @return Salário do funcionário.
    */
    string getSalario ();

    /**
    * @dbrief Método que atribui o salário do funcionário.
    * @param   s Recebe o salário para o funcionário.
    */
    void setSalario (string s);

    /**
    * @brief  Método para ter acesso a data de admissao do Funcionário.
    * @return Data de admissao do Funcionário.
    */
    string getData_admissao ();

    /**
    * @brief  Método que atribui a data de admissao do funcionário.
    * @param  a Recebe a data de admissao do funcionário.
    */
    void setData_admissao(string a);

    /**
    * @brief  Método que calcula o período de experiência do funcionário.
    * @param  d Recebe o dia que o funcionário foi contratado.
    * @param  m Recebe o mês que o funcionário foi contratado.
    * @param  a Recebe o ano que o funcionário foi contratado.
    * @return True ou False, se o funcionário estiver no periodo de experiência ou não.
    */
    bool Periodo_experiencia(int d, int m, int a);

    /**
    * @brief  Método que converte o salário do funcionário para facilitar a manipulação desse dado em outra função.
    * @return Valor do salário sem a vírgula.
    */
    string ConverterSalario ();

    /** 
    * @details O operador de inserção "<<" é sobrecarregado para que se possa escrever os valores no arquivo. 
    * @param	o Referência para stream de saida.
    * @param	f Referência para o objeto Funcionario a ser impresso.
    * @return	Referência para stream de saida.
    */
    friend ostream& operator << (ostream &o, Funcionario const f);

};

#endif