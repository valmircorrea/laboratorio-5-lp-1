/**
* @file	    menu.h
* @brief	Arquivo com o cabeçário da função menu.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	04/04/2017
* @date 	06/04/2017
*/

#include "empresa.h"

#ifndef MENU_H_
#define MENU_H_

/**
* @brief Função que apresenta o menu príncipal com as opções do programa.
* @return Opcao escolhida pelo usuario.
*/
int menu ();

#endif