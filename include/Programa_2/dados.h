/**
* @file	    dados.h
* @brief    
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	27/04/2017
* @date	    27/04/2017
*/

#include <cstdlib>

#ifndef DADOS_H_
#define DADOS_H_

class Dado {
    private:
    int dado[6];

    public:
    Dados::Dados () {
        dado[6] = {1, 2, 3, 4, 5, 6};
    }

    int lancar_dado (){
        srand(time(NULL));
        int ii = rand() % 6;

        return dado[ii];
    }

};

#endif