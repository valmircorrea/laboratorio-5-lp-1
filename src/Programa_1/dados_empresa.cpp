/**
* @file	    dados_empresa.cpp
* @brief	Arquivo com as funções de manipulação de dados das empresas.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	04/04/2017
* @date 	07/04/2017
*/

#include <iostream>
using std:: cin;
using std:: cout;
using std:: endl;

#include <fstream>
using std:: ifstream; 

#include <string>
using std:: string;

#include "funcionario.h"
#include "empresa.h"
#include "dados_empresa.h"

/**
* @brief  Função que cadastra uma nova empresa.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  *qtd_empresas Ponteiro de inteiro que recebe a quantidade de empresas cadastradas até o momento.
* @return  Retorna uma nova empresa já cadastrada.
*/
Empresa* cadastrar_empresa (Empresa *empresa, int *qtd_empresas) {

    int indice_aux = *qtd_empresas;
    empresa = add_empresa (empresa, qtd_empresas);

    string leitura;

    cout << "Cadastro da empresa:" << endl << endl;
    cout << "Digite o nome da empresa: ";
    cin >> leitura;
    empresa[indice_aux].setNome_empresa (leitura);

    cout << "Informe o CNPJ: ";
    cin >> leitura;
    empresa[indice_aux].setCNPJ (leitura);

    cout << endl << "Empresa '" << empresa[indice_aux].getNome_empresa() << "' cadastrada com sucesso!" << endl << endl;    

    cadastrar_funcionarios (empresa, indice_aux);

    return empresa;
}

/**
* @brief  Função que cadastra uma novo funcionário.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  indice_aux Variável que recebe um índice para saber em qual empresa cadastrar o novo funcionário.
* @return Retorna 0.
*/
int cadastrar_funcionarios ( Empresa *empresa, int indice_aux) {
    
    if (indice_aux == -1) {
        return 0;
    }

    string nome, salario, data, lixo, local = "./data/imput/funcionarios_";
    local += empresa[indice_aux].getNome_empresa() + ".csv"; 
    int qtd_funcionarios;    

    cout << "Informe a quantidade de funcionários: ";
    cin >> qtd_funcionarios;
    cout << endl;
    cout << "******************************************************* ATENÇÃO! *********************************************************";
    cout << endl << endl;
    cout << "Os funcionários serão cadastrados na empresa '" << empresa[indice_aux].getNome_empresa ();
    cout << "' por via de uma tabela em arquivo do tipo '.csv', o qual deve estar contido na pasta        './data/imput/'. ";
    cout << "Certifique - se de que o arquivo esteja nomeado da seguinte forma: funcionarios" << empresa[indice_aux].getNome_empresa () << ".csv, onde";
    cout << ", se exetirem espaços, devem ser preenchidos com o caracter '_'." << endl << endl; 
    cout << "Ex:" << endl << " Nome da empresa: Apple Brasil" << endl << " Nome do arquivo com funcionários: funcionarios_Apple_Brasil.csv" << endl << endl; 
    cout << "**************************************************************************************************************************";
    cout << endl << endl;
    
    int verificar;
    do {
        cout << "Padrão do arquivo esta de acordo com o solicitado ?" << endl << "1) SIM" << endl << "2) NÃO" << endl << endl;
        cin >> verificar;
        if (verificar == 2 ) {
            cout << endl << "Vá até a pasta './data/imput' e altere de acordo com o padrão de leitura!" << endl << endl;
        }
    } while (verificar != 1);
    
    ifstream entrada (local);

     if (!entrada) {
        cout << "Arquivo não encontrado!" << endl;
        if (qtd_funcionarios > 1) {
            cout << "funcionários(as) não foram cadastrados(as) para a empresa '" << empresa[indice_aux].getNome_empresa () << "'" << endl;
        } 
        else {
            cout << "funcionário(a) não foi cadastrado(a) para a empresa '" << empresa[indice_aux].getNome_empresa () << "'" << endl;            
        }
        return 0;
    }

    int funcionarios_cadastrados = 0;

    cout << endl;

    getline (entrada, lixo);

    for (int ii = 0; ii < qtd_funcionarios; ii++) {

        getline (entrada, lixo, '"');
        getline (entrada, nome, '"');

        getline (entrada, lixo, '"');
        getline (entrada, salario, '"');

        getline (entrada, lixo, '"');
        getline (entrada, data, '"');

        if (empresa[indice_aux].Verificar_funcionario (nome, salario, data) == true ) {
            cout << "Funcionario(a) '" << nome << "' já está cadastrado(a)!" << endl;
        }
        else {
            empresa[indice_aux].add_funcionario(nome, salario, data);
            funcionarios_cadastrados++;
        }
    }

    if (qtd_funcionarios > 1 && funcionarios_cadastrados > 1) {
        cout << endl << "Funcionários(as) cadastrados(as) com sucesso!" << endl;
    }
    else if (qtd_funcionarios > 1 && funcionarios_cadastrados == 0) {
        cout <<  endl << "Funcionários(as) não foram cadastrados(as) com sucesso!" << endl;
    }
    else if (funcionarios_cadastrados == 1) {
        cout << endl << "Funcionário(a) '" << nome << "' cadastrado(a) com sucesso!" << endl;        
    }
    else {
        cout << endl << "Funcionário(a) '" << nome << "' não foi cadastrado(a) com sucesso!" << endl;        
    }
    
    entrada.close();
    
    return 0;   
}

/**
* @brief  Função que adiciona uma nova empresa, manipulando adequadamente a alocação dinâmica.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  *qtd_empresas Ponteiro de inteiro que recebe a quantidade de empresas cadastradas até o momento.
* @return Retorna um novo vetor com um numero de empresas atualizado.
*/
Empresa* add_empresa (Empresa *empresa, int *qtd_empresas) {
    int aux = *qtd_empresas;
    *qtd_empresas += 1;

    if (aux > 0) {
        
        Empresa *emp_aux = new Empresa [aux];

        for (int ii = 0; ii < aux; ii++) {
            emp_aux [ii] = empresa [ii];
        }

        delete [] empresa;

        empresa = new Empresa [*qtd_empresas];

        for (int ii = 0; ii < aux; ii++) {
            empresa [ii] = emp_aux [ii];
        }

        delete [] emp_aux;
    }
    
    else {
        
        delete [] empresa;

        empresa = new Empresa [*qtd_empresas];
    }

    return empresa;
}

/**
* @brief  Função que realiza uma busca de uma determinada empresa no banco de dados.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  *qtd_empresas Ponteiro de inteiro que recebe a quantidade de empresas cadastradas até o momento.
* @return  Retorna o índice da posição de onde está a empresa solicitado pelo usuário.
*/
int Busca_empresa (Empresa *empresa, int n, int qtd_empresas) {
    string empresa_escolhida;
    if (n == 2) {
        cout << endl << "Deseja cadastrar funcionários para qual empresa?: ";
        cin >> empresa_escolhida;
        for (int ii = 0; ii < qtd_empresas; ii++) {
            if (empresa_escolhida.compare(empresa[ii].getNome_empresa()) == 0) {
                return ii;
            }
        }
    }
    else if (n == 4 || n == 5) {
        cout << endl << "Deseja listar funcionários de qual empresa?: ";
        cin >> empresa_escolhida;
        for (int ii = 0; ii < qtd_empresas; ii++) {
            if (empresa_escolhida.compare(empresa[ii].getNome_empresa()) == 0) {
                return ii;
            }
        } 
    }

    else if (n == 6) {
        cout << endl << "Deseja aplicar o aumento para os funcionários de qual empresa?: ";
        cin >> empresa_escolhida;
        for (int ii = 0; ii < qtd_empresas; ii++) {
            if (empresa_escolhida.compare(empresa[ii].getNome_empresa()) == 0) {
                return ii;
            }
        } 
    }
    else {
        cout << endl << "Deseja deseja salvar dados de qual empresa?: ";
        cin >> empresa_escolhida;
        for (int ii = 0; ii < qtd_empresas; ii++) {
            if (empresa_escolhida.compare(empresa[ii].getNome_empresa()) == 0) {
                return ii;
            }
        }
    }

    cout << endl << "Empresa '" << empresa_escolhida << "' não foi cadastrada!" << endl;    

    return -1;
}

/**
* @brief   Função que lista as empresas cadastradas pelo usuário.
* @param  *empresa Ponteiro de classe que recebe uma empresa como objeto.
* @param  *qtd_empresas Ponteiro de inteiro que recebe a quantidade de empresas cadastradas até o momento.
*/
void Listar_empresas (Empresa *empresa, int qtd_empresas) {

    if (qtd_empresas == 0) {
        cout << "Não há empresas cadastradas até o momento!" << endl;
    }
    else {
        if (qtd_empresas == 1) {
            cout << "Existe apenas " << qtd_empresas << " empresa cadastrada no momento:" << endl;
        }
        else {
            cout << "Existem " << qtd_empresas << " empresas cadastradas no momento:" << endl;            
        }
        for (int ii = 0; ii < qtd_empresas; ii++) {
            cout << endl;
            cout << "--------------------------------------" << endl; 
            cout << "Nome: " << empresa[ii].getNome_empresa() << endl;
            cout << "CNPJ: " << empresa[ii].getCNPJ () << endl;
            cout << "--------------------------------------" << endl;
        }
    }
}