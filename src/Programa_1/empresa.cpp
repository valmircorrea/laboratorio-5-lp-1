/**
* @file	    empresa.cpp
* @brief	Implementacao dos metodos da classe Empresa.
* @author   Valmir Correa (valmircorrea96@outlook.com) 
* @since	04/05/2017
* @date	    07/05/2017
* @sa		empresa.h
*/

#include <iostream>
using std:: cin;
using std:: cout;
using std:: endl;

#include <string>
using std:: string;

#include <fstream>
using std::ofstream;

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>

#include "funcionario.h"
#include "empresa.h"

/**
* @brief Contrutor para a classe Empresa.
*/
Empresa::Empresa () {
    nome_empresa = " ";
    CNPJ = " ";
    lista_funcionarios = NULL;
    quant = 0;
}

/**
* @brief Método para ter acesso ao nome da empresa.
* @return Nome da empresa
*/
string Empresa::getNome_empresa () {
    return nome_empresa;
}

/**
* @brief  Método que atribui o nome da empresa.
* @param  n Recebe o nome da empresa.
*/
void Empresa::setNome_empresa (string n) {
    nome_empresa = n;
}

/**
* @brief Método para ter acesso ao CNPJ da empresa.
* @return CNPJ da empresa
*/
string Empresa::getCNPJ () {
    return CNPJ;
}

/**
* @brief  Método que atribui o CNPJ da empresa.
* @param  n Recebe o CNPJ da empresa.
*/
void Empresa::setCNPJ (string d) {
    CNPJ = d;
}

/**
* @brief  Método que gera a lista de funcionários da empresa.
* @param  opcao Variável que recebe a opção do usuário em relação ao tipo de filtro que será aplicado na geração da lista.
*/
void Empresa::getLista_funcionarios (int opcao) {
    if (opcao == 5) {

        time_t timer;
        struct tm *horarioLocal;

        time(&timer); // Obtem informações de data e hora
        horarioLocal = localtime(&timer); // Converte a hora atual para a hora local

        int dia = horarioLocal->tm_mday;
        int mes = horarioLocal->tm_mon + 1;
        int ano = horarioLocal->tm_year + 1900;

        FuncionarioExperiencia (dia, mes, ano);

    }
    else {
        if (quant != 0) {
            for (int ii = 0; ii < quant; ii++) {
                cout << "Nome: " << lista_funcionarios[ii].getNome() << endl;
                cout << "Salário: R$ " << lista_funcionarios[ii].getSalario() << endl;
                cout << "Data de admissao: " << lista_funcionarios[ii].getData_admissao() << endl << endl;
            }
        }
        else {
            cout << "Não há funcionários cadastrados !" << endl;
        }
    }
}

/**
* @brief  Método que adiciona um novo funcionário a lista de funcionários da empresa.
* @param  novo_funcionario Variável que recebe a nova quantidade de funcionarios.
*/
void Empresa::setLista_funcionarios(int novo_funcionario) {

    if (quant > 0) {
        
        Funcionario *empregados_temp = new Funcionario [quant];

        for (int ii = 0; ii < quant ; ii++) {
            empregados_temp [ii].setNome( lista_funcionarios[ii].getNome() );
            empregados_temp [ii].setSalario( lista_funcionarios[ii].getSalario() );
            empregados_temp [ii].setData_admissao( lista_funcionarios[ii].getData_admissao() );
        }

        delete [] lista_funcionarios;

        lista_funcionarios = new Funcionario [novo_funcionario];    

        for (int ii = 0; ii < quant; ii++) {
            lista_funcionarios [ii].setNome( empregados_temp [ii].getNome() );
            lista_funcionarios [ii].setSalario( empregados_temp [ii].getSalario() );
            lista_funcionarios [ii].setData_admissao( empregados_temp [ii].getData_admissao() );
        }

        delete [] empregados_temp;
    }
    else {
        delete [] lista_funcionarios;

        lista_funcionarios = new Funcionario [novo_funcionario];
    }
    quant = novo_funcionario;
}

/**
* @brief  Método que adiciona um novo funcionário a empresa.
* @param  n Recebe o nome do novo funcionário contratado.
* @param  s Recebe o salário do novo funcionário contratado.
* @param  d Recebe a data de admissão do novo funcionário contratado.
*/
void Empresa::add_funcionario(string n, string s, string d) {

    int aux = quant + 1;

    setLista_funcionarios(aux);

    lista_funcionarios[quant-1].setNome (n); //1
    lista_funcionarios[quant-1].setSalario (s);
    lista_funcionarios[quant-1].setData_admissao (d);
}

/**
* @brief  Método que verifica se o novo funcionário já foi cadastrado na empresa anteriormente.
* @param  n Recebe o nome do novo funcionário contratado.
* @param  s Recebe o salário do novo funcionário contratado.
* @param  d Recebe a data de admissão do novo funcionário contratado.
* @return True ou False, se o funcionário já foi ou não cadastrado.
*/
bool Empresa::Verificar_funcionario (string n, string s, string d) {
    
    Funcionario aux;
    
    aux.setNome(n);
    aux.setData_admissao(d);

    for (int ii = 0; ii < quant; ii++) {
        if ( aux.getNome() == lista_funcionarios[ii].getNome() ) {
            if ( aux.getData_admissao() == lista_funcionarios[ii].getData_admissao() ) {
                return true;
            }
        }
    }
    return false;
}

/**
* @brief  Método que busca e imprime os funcionários que estão em período de experiência.
* @param  d Recebe o dia que o funcionário foi contratado.
* @param  m Recebe o mês que o funcionário foi contratado.
* @param  a Recebe o ano que o funcionário foi contratado.
*/
void Empresa::FuncionarioExperiencia (int d, int m, int a) {

    int aux = 0;

    for (int ii = 0; ii < quant; ii++) {

        if (lista_funcionarios[ii].Periodo_experiencia (d, m, a) == true) {
            cout << "Nome: " << lista_funcionarios[ii].getNome() << endl;
            cout << "Salário: R$ " << lista_funcionarios[ii].getSalario() << endl;
            cout << "Data de admissao: " << lista_funcionarios[ii].getData_admissao() << endl << endl;
        }
        else {
            aux += 1;            
        } 
    }

    if (aux == quant) {
        cout << "Nenhum funcionário em período de experiência!" << endl;        
    }
}

/**
* @brief  Método que busca e imprime os funcionários que estão em período de experiência.
* @param  nome Recebe o nome da empresa que o usuário quer a aplicação do aumento para os funcionários.
* @param  n Recebe um valor para verificar se a empresa que o usuário buscou foi realmente cadastrada.
*/
void Empresa::Aumento_funcionarios (string nome, int n) {

    if (n == -1) {
        cout << " ";
    }

    else {

        if (quant < 1) {
            cout << "Não há funcionários na empresa '" << nome << "'" << endl;
        }
        else {

            float aumento;

            cout << "Deseja aplicar um aumento de quantos porcento para os funcionários?: ";
            cin >> aumento;        

            double aux = (double) aumento / (double) 100;

            for (int jj = 0; jj < quant; jj++) {
                
                string s_salario = lista_funcionarios[jj].ConverterSalario();

                s_salario += ".00";
                double i_salario = atof (s_salario.c_str());
                double antigo_salario = i_salario;
                double novo_salario = (antigo_salario * aux) + antigo_salario;
                
                string s_novo_salario = "";
                s_novo_salario += std::to_string (novo_salario);
                string s_novo_salario_com_2_zeros = "";
                for (unsigned int pp = 0; pp < strlen(s_novo_salario.c_str()); pp++ ) {
                    if (s_novo_salario[pp] != '.') {
                        s_novo_salario_com_2_zeros += s_novo_salario[pp];
                    }
                    else {
                        break;
                    }
                }
                s_novo_salario_com_2_zeros += ",00";
                lista_funcionarios[jj].setSalario (s_novo_salario_com_2_zeros);
            }

            cout << endl <<"Aplicado o aumento nos salários dos funcionários(as) da empresa '" << nome <<"'" << endl;
        }
    }
}

/**
* @brief  Método que gera um arquivo com todos os dados da empresa, especificada pelo usuário, e de seus funcionários. 
* @param  nome Recebe o nome da empresa que o usuário quer que seja feita a geração do arquivos de dados.
* @param  n Recebe um valor para verificar se a empresa que o usuário buscou foi realmente cadastrada.
*/
void Empresa::Salvar_dados (string nome, int n) {

    if (n == -1) {
        cout << " ";
    }
    else {
        string nome_arq = "./data/output/" + nome + ".csv";

        ofstream saida (nome_arq);

        saida << "Nome da empresa: " << nome_empresa << endl;
        saida << "CNPJ: " << CNPJ << endl << endl;
        saida << "Nome; Salário; Data de admissão" << endl;

        for (int ii = 0; ii < quant; ii++) {
            saida << lista_funcionarios[ii];
        }

        cout << "Dados da empresa '" << nome << "' foram salvos em '" << nome_arq << "' com sucesso!";
    }
}

/**
* @brief Destrutor padrão para liberar o espaço na memória.
*/
Empresa::~Empresa () {
    if (quant > 0)
        delete [] lista_funcionarios;
}