/**
* @file	    funcionario.cpp
* @brief	Implementacao dos metodos da classe Funcionario
* @author   Valmir Correa (valmircorrea96@outlook.com) 
* @since	04/05/2017
* @date	    07/05/2017
* @sa		funcionario.h
*/

#include <string>
using std:: string;

#include <ostream>
using std::ostream;

#include <iostream>
using std:: endl;
using std:: cout;

#include <cstring>

#include "funcionario.h"

/**
* @brief Contrutor para a classe Funcionário.
*/
Funcionario::Funcionario() {
    nome = " ";
    salario = " ";
    data_admissao = " ";
}

/**
* @brief Método para ter acesso ao nome do Funcionário.
* @return Nome do funcionario 
*/
string Funcionario::getNome () {
    return nome;
}

/**
* @brief   Método que atribui o nome do funcionario.
* @param   n Recebe o nome do funcionário.
*/
void Funcionario::setNome (string n) {
    nome = n;
}

/**
* @brief  Método para ter acesso ao salário do Funcionário.
* @return Salário do funcionário.
*/
string Funcionario::getSalario () {
    return salario;
}

/**
* @dbrief Método que atribui o salário do funcionário.
* @param   s Recebe o salário para o funcionário.
*/
void Funcionario::setSalario (string s) {
    salario = s;
}

/**
* @brief  Método para ter acesso a data de admissao do Funcionário.
* @return Data de admissao do Funcionário.
*/
string Funcionario::getData_admissao () {
    return data_admissao;
}

/**
* @brief  Método que atribui a data de admissao do funcionário.
* @param  a Recebe a data de admissao do funcionário.
*/
void Funcionario::setData_admissao (string a) {
    data_admissao = a;
}

/**
* @brief  Método que calcula o período de experiência do funcionário.
* @param  d Recebe o dia que o funcionário foi contratado.
* @param  m Recebe o mês que o funcionário foi contratado.
* @param  a Recebe o ano que o funcionário foi contratado.
* @return True ou False, se o funcionário estiver no periodo de experiência ou não.
*/
bool Funcionario::Periodo_experiencia(int d, int m, int a) {

    int dias_mes[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    string s_dia, s_mes, s_ano;

    unsigned int ii = 0;
    for (; ii < strlen(data_admissao.c_str()); ii++) {
        if (data_admissao[ii] != '/') {
            s_dia += data_admissao[ii];
        }
        else {
            ii++;
            break;
        }
    }
    for (; ii < strlen(data_admissao.c_str()); ii++) {
        if (data_admissao[ii] != '/') {
            s_mes += data_admissao[ii];
        }
        else {
            ii++;
            break;
        }
    }
    for (; ii < strlen(data_admissao.c_str()); ii++) {
        s_ano += data_admissao[ii];
    }

    int dia = atoi(s_dia.c_str());
    int mes = atoi(s_mes.c_str());
    int ano = atoi(s_ano.c_str());

    int data = dia + (mes * dias_mes[mes - 1]) + (ano * 365);
    int data2 = d + (m * dias_mes[m - 1]) + (a * 365);

    if (data2 - data < 90) {
        return true;
    }

    return false;
}

/**
* @brief  Método que converte o salário do funcionário para facilitar a manipulação desse dado em outra função.
* @return Valor do salário sem a vírgula.
*/
string Funcionario::ConverterSalario () {
    string s_salario = "";
    for (unsigned int ii = 0; ii < strlen(salario.c_str()); ii++ ) {
        if (salario[ii] != ',') {
            s_salario += salario[ii];
        }
        else {
            break;
        }
    }
    return s_salario;
}

/** 
 * @details O operador de inserção "<<" é sobrecarregado para que se possa escrever os valores no arquivo. 
 * @param	o Referência para stream de saida.
 * @param	f Referência para o objeto Funcionario a ser impresso.
 * @return	Referência para stream de saida.
 */
ostream& operator << (ostream &o, Funcionario const f) {
    string s_novo_salario_sem_virgula = "";
    for (unsigned int pp = 0; pp < strlen(f.salario.c_str()); pp++ ) {
        if (f.salario[pp] != ',') {
            s_novo_salario_sem_virgula += f.salario[pp];
        }
        else {
            break;
        }
    }

    o << f.nome << ";" << "R$ " << s_novo_salario_sem_virgula << ";"<< f.data_admissao << endl;

    return o;
}