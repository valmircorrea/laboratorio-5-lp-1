/**
* @file	    main.cpp
* @brief	Arquivo com a função principal.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	27/04/2017
* @date 	07/04/2017
*/

#include <iostream>
using std:: cin;
using std:: cout;
using std:: endl;

#include <string>
using std:: string;

#include "funcionario.h"
#include "empresa.h"
#include "dados_empresa.h"
#include "menu.h"

/** 
 * @brief Função principal
 */
int main () {

    Empresa *p_empresa = NULL;
    int opcao, qtd_empresas = 0;

    int indice_aux;

    do {
        opcao = menu();

        switch (opcao){
            case 1:
                p_empresa = cadastrar_empresa (p_empresa, &qtd_empresas);
                break;
            case 2: 
                indice_aux = Busca_empresa (p_empresa, opcao, qtd_empresas);
                cadastrar_funcionarios (p_empresa, indice_aux);
                break;
            case 3:
                Listar_empresas (p_empresa, qtd_empresas);
                break;
            case 4:
                indice_aux = Busca_empresa (p_empresa, opcao, qtd_empresas);
                cout << endl << "------------ Lista de funcionarios da empresa '" << p_empresa[indice_aux].getNome_empresa() << "' -------------" << endl << endl;
                p_empresa[indice_aux].getLista_funcionarios (opcao);
                cout << endl << "---------------------------------------------------------------------------------------------------------------";
                break;
            case 5:
                indice_aux = Busca_empresa (p_empresa, opcao, qtd_empresas);
                cout << endl << "------------ Lista de funcionarios em período de experiência da empresa '" << p_empresa[indice_aux].getNome_empresa() << "' -------------" << endl << endl;
                p_empresa[indice_aux].getLista_funcionarios (opcao);
                break;
            case 6:
                indice_aux = Busca_empresa (p_empresa, opcao, qtd_empresas);
                p_empresa[indice_aux].Aumento_funcionarios(p_empresa[indice_aux].getNome_empresa(), indice_aux);
                break;
            case 7:
                indice_aux = Busca_empresa (p_empresa, opcao, qtd_empresas);
                p_empresa[indice_aux].Salvar_dados(p_empresa[indice_aux].getNome_empresa(), indice_aux);
                break;
            default:
                cout << endl << "Programa encerrado" << endl;
                cout << "Created by Valmir Correa" << endl;
                exit(1);
        }

    } while (opcao != 0);

    delete [] p_empresa;

    return 0;
}