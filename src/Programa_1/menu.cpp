/**
* @file	    menu.cpp
* @brief	Arquivo com a função menu.
* @author   Valmir Correa (valmircorrea96@outlook.com)
* @since	04/04/2017
* @date 	06/04/2017
*/

#include <iostream>
using std:: cin;
using std:: cout;
using std:: endl;

#include <string>
using std:: string;

#include "empresa.h"
#include "menu.h"

/**
* @brief Função que apresenta o menu príncipal com as opções do programa.
* @return Opcao escolhida pelo usuario.
*/
int menu () {
    
    int escolha;
    cout << endl << endl << "--------------------------- Menu principal --------------------------" << endl << endl;
    cout << "1) Cadastrar uma empresa" << endl;
    cout << "2) Adicionar funcionários" << endl;
    cout << "3) Listar empresas" << endl;
    cout << "4) Listar dados de funcionários de uma determinada empresa" << endl;
    cout << "5) Listar dados de funcionários em período de experiência, em uma determinada empresa" << endl;    
    cout << "6) Aumento para os funcionários" << endl;
    cout << "7) Salvar dados em arquivo" << endl << endl;    
    cout << "0) Sair do programa" << endl << endl;
    cout << "-> ";

    cin >> escolha;

    return escolha;
}